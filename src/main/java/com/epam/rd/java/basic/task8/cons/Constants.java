package com.epam.rd.java.basic.task8.cons;

public final class Constants {
    public static final String XML_FILE = "input.xml";
    public static final String XSD_FILE = "input.xsd";
    public static final String DOM_XML_OUTPUT = "output.dom.xml";
    public static final String STAX_XML_OUTPUT = "output.stax.xml";
    public static final String SAX_XML_OUTPUT = "output.sax.xml";

    public static final int FIRST_ELEMENT_INDEX = 0;
    public static final String CLASS_XML_SCHEMA_FACTORY_INTERNAL =
            "com.sun.org.apache.xerces.internal.jaxp.validation.XMLSchemaFactory";
    public static final String CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL =
            "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl";
    public static final String CLASS_SAX_PARSER_FACTORY_INTERNAL =
            "com.sun.org.apache.xerces.internal.jaxp.SAXParserFactoryImpl";

    // your own namespace
    public static final String MY_NS__URI = "http://www.nure.ua";
    public static final String MY_NS__PREFIX = "xsi";

    // for schema location
    public static final String SCHEMA_LOCATION__URI =
            "http://www.nure.ua input.xsd";
    public static final String SCHEMA_LOCATION__ATTR_FQN = "xsi:schemaLocation";
}
