package com.epam.rd.java.basic.task8.utils;

import com.epam.rd.java.basic.task8.cons.Constants;
import com.epam.rd.java.basic.task8.cons.XML;
import com.epam.rd.java.basic.task8.controller.DOMController;
import com.epam.rd.java.basic.task8.entity.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class XMLSaver {
    public void saveXML(Flowers flowers, String fileName) throws ParserConfigurationException, TransformerException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(
                Constants.CLASS_DOCUMENT_BUILDER_FACTORY_INTERNAL,
                DOMController.class.getClassLoader());
        dbf.setNamespaceAware(true);
        dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.newDocument();

        Element tElement = document.createElement(XML.FLOWERS.value());

        tElement.setAttributeNS(
                XMLConstants.W3C_XML_SCHEMA_INSTANCE_NS_URI,
                Constants.SCHEMA_LOCATION__ATTR_FQN,
                Constants.SCHEMA_LOCATION__URI);

        tElement.setAttribute("xmlns", Constants.MY_NS__URI);

        document.appendChild(tElement);

        for (Flowers.Flower flower : flowers.getFlower()) {
            Element flowerElement = document.createElement(XML.FLOWER.value());
            tElement.appendChild(flowerElement);

            Element nameElement = document.createElement(XML.NAME.value());
            nameElement.setTextContent(String.valueOf(flower.getName()));
            flowerElement.appendChild(nameElement);

            Element soilElement = document.createElement(XML.SOIL.value());
            soilElement.setTextContent(flower.getSoil());
            flowerElement.appendChild(soilElement);

            Element originElement = document.createElement(XML.ORIGIN.value());
            originElement.setTextContent(flower.getOrigin());
            flowerElement.appendChild(originElement);

            Element visualParametersElement = document.createElement(XML.VISUALPARAMETERS.value());
            Element stemColourElement = document.createElement(XML.STEMCOLOUR.value());
            stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
            visualParametersElement.appendChild(stemColourElement);
            Element leafColourElement = document.createElement(XML.LEAFCOLOUR.value());
            leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
            visualParametersElement.appendChild(leafColourElement);
            Element aveLenFlowerElement = document.createElement(XML.AVELENFLOWER.value());
            aveLenFlowerElement.setAttribute(XML.MEASURE.value(), flower.getVisualParameters().getAveLenFlower().getMeasure());
            aveLenFlowerElement.setTextContent(flower.getVisualParameters().getAveLenFlower().getValue().toString());
            visualParametersElement.appendChild(aveLenFlowerElement);

            flowerElement.appendChild(visualParametersElement);

            Element growingTipsElement = document.createElement(XML.GROWINGTIPS.value());

            Element temperatureElement = document.createElement(XML.TEMPRETURE.value());
            temperatureElement.setTextContent(flower.getGrowingTips().getTempreture().getValue().toString());
            temperatureElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getTempreture().getMeasure());
            growingTipsElement.appendChild(temperatureElement);

            Element lightingElement = document.createElement(XML.LIGHTING.value());
            lightingElement.setAttribute(XML.LIGHTREQUIRING.value(), flower.getGrowingTips().getLighting().getLightRequiring());
            growingTipsElement.appendChild(lightingElement);

            Element wateringElement = document.createElement(XML.WATERING.value());
            wateringElement.setTextContent(flower.getGrowingTips().getWatering().getValue().toString());
            wateringElement.setAttribute(XML.MEASURE.value(), flower.getGrowingTips().getWatering().getMeasure());
            growingTipsElement.appendChild(wateringElement);

            flowerElement.appendChild(growingTipsElement);

            Element multiplyingElement = document.createElement(XML.MULTIPLYING.value());
            multiplyingElement.setTextContent(flower.getMultiplying().toString());
            flowerElement.appendChild(multiplyingElement);
        }

        saveToXML(document, fileName);
    }

    private static void saveToXML(Document document, String xmlFileName) throws TransformerException {
        StreamResult result = new StreamResult(xmlFileName);

        TransformerFactory tf = TransformerFactory.newInstance();
        tf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");

        t.transform(new DOMSource(document), result);
    }
}
