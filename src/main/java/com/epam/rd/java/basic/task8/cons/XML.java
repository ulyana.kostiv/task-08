package com.epam.rd.java.basic.task8.cons;

public enum XML {
    FLOWERS("flowers"),
    FLOWER("flower"),
    NAME("name"),
    SOIL("soil"),
    ORIGIN("origin"),
    VISUALPARAMETERS("visualParameters"),
    STEMCOLOUR("stemColour"),
    LEAFCOLOUR("leafColour"),
    AVELENFLOWER("aveLenFlower"),
    MEASURE("measure"),
    GROWINGTIPS("growingTips"),
    TEMPRETURE("tempreture"),
    LIGHTING("lighting"),
    WATERING("watering"),
    LIGHTREQUIRING("lightRequiring"),
    MULTIPLYING("multiplying");

    private final String value;

    XML(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }

    public String value(String prefix) {
        return prefix + ":" + value;
    }
}
