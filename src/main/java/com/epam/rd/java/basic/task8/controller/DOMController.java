package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.cons.XML;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.Flowers.Flower;
import com.epam.rd.java.basic.task8.entity.ObjectFactory;
import com.epam.rd.java.basic.task8.utils.XMLSaver;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Optional;

import static com.epam.rd.java.basic.task8.cons.Constants.DOM_XML_OUTPUT;
import static com.epam.rd.java.basic.task8.cons.Constants.FIRST_ELEMENT_INDEX;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private String xmlFileName;
	private Flowers flowers;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws ParserConfigurationException, IOException, SAXException {
		String factoryClassName = "com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl";
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance(factoryClassName, this.getClass().getClassLoader());
		dbf.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		db.setErrorHandler(new DefaultHandler() {
			@Override
			public void error(SAXParseException e) throws SAXException {
				throw e;
			}
		});
		Document document = db.parse(xmlFileName);
		Element root = document.getDocumentElement();
		flowers = ObjectFactory.createFlowers();
		NodeList flowerNodes = root.getElementsByTagName(XML.FLOWER.value());

		for (int i = 0; i < flowerNodes.getLength(); i++) {
			Flower flower = getFlower(flowerNodes.item(i));
			flowers.getFlower().add(flower);
		}
	}

	private Flower getFlower(Node flowerNode) {
		Flower flower  = ObjectFactory.createFlowersFlower();

		Element flowerElement = (Element) flowerNode;
		Node nameNode = flowerElement.getElementsByTagName(XML.NAME.value()).item(FIRST_ELEMENT_INDEX);
		flower.setName(nameNode.getTextContent());
		Node soilNode = flowerElement.getElementsByTagName(XML.SOIL.value()).item(FIRST_ELEMENT_INDEX);
		flower.setSoil(soilNode.getTextContent());
		Node originNode = flowerElement.getElementsByTagName(XML.ORIGIN.value()).item(FIRST_ELEMENT_INDEX);
		flower.setOrigin(originNode.getTextContent());
		Node multiplyingNode = flowerElement.getElementsByTagName(XML.MULTIPLYING.value()).item(FIRST_ELEMENT_INDEX);
		flower.setMultiplying(multiplyingNode.getTextContent());
		Node visualParametersNode = flowerElement.getElementsByTagName(XML.VISUALPARAMETERS.value()).item(FIRST_ELEMENT_INDEX);
		Flower.VisualParameters visualParameters = new Flower.VisualParameters();
		Element parametersElement = (Element) visualParametersNode;

		Element stemElement = (Element) parametersElement.getElementsByTagName(XML.STEMCOLOUR.value()).item(FIRST_ELEMENT_INDEX);
		Element leafElement = (Element) parametersElement.getElementsByTagName(XML.LEAFCOLOUR.value()).item(FIRST_ELEMENT_INDEX);
		Element avelenElement = (Element) parametersElement.getElementsByTagName(XML.AVELENFLOWER.value()).item(FIRST_ELEMENT_INDEX);

		Flower.VisualParameters.AveLenFlower avelen = new Flower.VisualParameters.AveLenFlower();
		avelen.setValue(new BigInteger(avelenElement.getTextContent()));
		avelen.setMeasure(avelenElement.getAttribute(XML.MEASURE.value()));
		visualParameters.setStemColour(stemElement.getTextContent());
		visualParameters.setLeafColour(leafElement.getTextContent());
		visualParameters.setAveLenFlower(avelen);

		flower.setVisualParameters(visualParameters);

		Flower.GrowingTips growingTips = new Flower.GrowingTips();

		Element growingTipsElement = (Element) flowerElement.getElementsByTagName(XML.GROWINGTIPS.value()).item(FIRST_ELEMENT_INDEX);
		Element temperatureElement = (Element) growingTipsElement.getElementsByTagName(XML.TEMPRETURE.value()).item(FIRST_ELEMENT_INDEX);
		Element lightingElement = (Element) growingTipsElement.getElementsByTagName(XML.LIGHTING.value()).item(FIRST_ELEMENT_INDEX);
		Element wateringElement = (Element) growingTipsElement.getElementsByTagName(XML.WATERING.value()).item(FIRST_ELEMENT_INDEX);

		Flower.GrowingTips.Tempreture tempreture = new Flower.GrowingTips.Tempreture();
		tempreture.setMeasure(temperatureElement.getAttribute(XML.MEASURE.value()));
		tempreture.setValue(new BigInteger(temperatureElement.getTextContent()));

		Flower.GrowingTips.Lighting lighting = new Flower.GrowingTips.Lighting();
		lighting.setLightRequiring(lightingElement.getAttribute(XML.LIGHTREQUIRING.value()));

		Flower.GrowingTips.Watering watering = new Flower.GrowingTips.Watering();
		watering.setMeasure(wateringElement.getAttribute(XML.MEASURE.value()));
		watering.setValue(new BigInteger(wateringElement.getTextContent()));

		growingTips.setTempreture(tempreture);
		growingTips.setLighting(lighting);
		growingTips.setWatering(watering);

		flower.setGrowingTips(growingTips);

		return flower;
	}

	public Flowers getFlowers() {
		return flowers;
	}

	public void saveXML(Flowers flowers, String xmlFileName) throws TransformerException, ParserConfigurationException {
		new XMLSaver().saveXML(flowers, xmlFileName);
	}
}
