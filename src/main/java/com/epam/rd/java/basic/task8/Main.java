package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.utils.Sorter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		domController.parse();

		Flowers flowers = domController.getFlowers();

		Sorter.sortFlowersByName(flowers);
		
		// save
		String outputXmlFile = "output.dom.xml";
		domController.saveXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse();

		flowers = saxController.getFlowers();

		Sorter.sortFlowersByMultiplying(flowers);
		
		// save
		outputXmlFile = "output.sax.xml";
		saxController.saveXML(flowers, outputXmlFile);
		
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();

		flowers = staxController.getFlowers();

		Sorter.sortFlowersByOrigin(flowers);
		
		// save
		outputXmlFile = "output.stax.xml";
		staxController.saveXML(flowers, outputXmlFile);
	}

}
