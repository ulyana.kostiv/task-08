package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.cons.Constants;
import com.epam.rd.java.basic.task8.cons.XML;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.ObjectFactory;
import com.epam.rd.java.basic.task8.utils.XMLSaver;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.lang.model.element.Name;
import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private String xmlFileName;
	private Flowers flowers;
	private String currentElement;
	private Flowers.Flower flower;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws SAXException, ParserConfigurationException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance(
				Constants.CLASS_SAX_PARSER_FACTORY_INTERNAL,
				this.getClass().getClassLoader());

		factory.setNamespaceAware(true);
		factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

		javax.xml.parsers.SAXParser parser = factory.newSAXParser();
		parser.parse(xmlFileName, this);
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		currentElement = localName;

		if (localName.equals(XML.FLOWERS.value())) {
			flowers = ObjectFactory.createFlowers();
		}

		if (localName.equals(XML.FLOWER.value())) {
			flower = ObjectFactory.createFlowersFlower();
		}

		if(localName.equals(XML.VISUALPARAMETERS.value())) {
			flower.setVisualParameters(new Flowers.Flower.VisualParameters());
		}

		if (localName.equals(XML.AVELENFLOWER.value())) {
			Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
			aveLenFlower.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));

			flower.getVisualParameters().setAveLenFlower(aveLenFlower);
		}

		if (localName.equals(XML.GROWINGTIPS.value())) {
			flower.setGrowingTips(new Flowers.Flower.GrowingTips());
		}

		if (localName.equals(XML.TEMPRETURE.value())) {
			Flowers.Flower.GrowingTips.Tempreture tempreture = new Flowers.Flower.GrowingTips.Tempreture();
			tempreture.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));

			flower.getGrowingTips().setTempreture(tempreture);
		}

		if (localName.equals(XML.LIGHTING.value())) {
			Flowers.Flower.GrowingTips.Lighting lighting = new Flowers.Flower.GrowingTips.Lighting();
			lighting.setLightRequiring(attributes.getValue(XML.LIGHTREQUIRING.value()));

			flower.getGrowingTips().setLighting(lighting);
		}

		if (localName.equals(XML.WATERING.value())) {
			Flowers.Flower.GrowingTips.Watering watering = new Flowers.Flower.GrowingTips.Watering();
			watering.setMeasure(attributes.getValue(uri, XML.MEASURE.value()));

			flower.getGrowingTips().setWatering(watering);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		String elementText = new String(ch, start, length).trim();

		if (elementText.isEmpty()) {
			return;
		}

		if (currentElement.equals(XML.NAME.value())) {
			flower.setName(elementText);
		}

		if (currentElement.equals(XML.SOIL.value())) {
			flower.setSoil(elementText);
		}

		if (currentElement.equals(XML.ORIGIN.value())) {
			flower.setOrigin(elementText);
		}

		if (currentElement.equals(XML.STEMCOLOUR.value())){
			flower.getVisualParameters().setStemColour(elementText);
		}

		if (currentElement.equals(XML.LEAFCOLOUR.value())){
			flower.getVisualParameters().setLeafColour(elementText);
		}

		if (currentElement.equals(XML.AVELENFLOWER.value())) {
			flower.getVisualParameters().getAveLenFlower().setValue(new BigInteger(elementText));
		}

		if (currentElement.equals(XML.TEMPRETURE.value())) {
			flower.getGrowingTips().getTempreture().setValue(new BigInteger(elementText));
		}

		if (currentElement.equals(XML.WATERING.value())) {
			flower.getGrowingTips().getWatering().setValue(new BigInteger(elementText));
		}

		if (currentElement.equals(XML.MULTIPLYING.value())) {
			flower.setMultiplying(elementText);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		if (localName.equals(XML.FLOWER.value())) {
			flowers.getFlower().add(flower);
		}
	}
	
	public Flowers getFlowers() {
		return flowers;
	}
	
	public void saveXML(Flowers flowers, String xmlFileName) throws TransformerException, ParserConfigurationException {
		new XMLSaver().saveXML(flowers, xmlFileName);
	}

}