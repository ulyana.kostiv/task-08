package com.epam.rd.java.basic.task8.utils;

import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Comparator;

public class Sorter {
    public static void sortFlowersByName(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getName));
    }

    public static void sortFlowersByMultiplying(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getMultiplying));
    }

    public static void sortFlowersByOrigin(Flowers flowers) {
        flowers.getFlower().sort(Comparator.comparing(Flowers.Flower::getOrigin));
    }
}
