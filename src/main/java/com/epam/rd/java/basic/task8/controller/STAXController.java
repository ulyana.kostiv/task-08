package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.cons.XML;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.entity.ObjectFactory;
import com.epam.rd.java.basic.task8.utils.XMLSaver;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flowers flowers;
	private String currentElement;
	private Flowers.Flower flower;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public void parse() throws XMLStreamException {
		XMLInputFactory factory = XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
		factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);

		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));

		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();

			if (event.isCharacters() && event.asCharacters().isWhiteSpace()) {
				continue;
			}

			if (event.isStartElement()) {
				parseStartElement(event.asStartElement());
			} else if (event.isCharacters()) {
				parseContent(event.asCharacters());
			} else if (event.isEndElement()) {
				parseEndElement(event.asEndElement());
			}
		}

		reader.close();
	}

	private void parseStartElement(StartElement startElement) {
		currentElement = startElement.getName().getLocalPart();

		if (currentElement.equals(XML.FLOWERS.value())) {
			flowers = ObjectFactory.createFlowers();
		}

		if (currentElement.equals(XML.FLOWER.value())) {
			flower = ObjectFactory.createFlowersFlower();
		}

		if(currentElement.equals(XML.VISUALPARAMETERS.value())) {
			flower.setVisualParameters(new Flowers.Flower.VisualParameters());
		}

		if (currentElement.equals(XML.AVELENFLOWER.value())) {
			Flowers.Flower.VisualParameters.AveLenFlower aveLenFlower = new Flowers.Flower.VisualParameters.AveLenFlower();
			aveLenFlower.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());

			flower.getVisualParameters().setAveLenFlower(aveLenFlower);
		}

		if (currentElement.equals(XML.GROWINGTIPS.value())) {
			flower.setGrowingTips(new Flowers.Flower.GrowingTips());
		}

		if (currentElement.equals(XML.TEMPRETURE.value())) {
			Flowers.Flower.GrowingTips.Tempreture tempreture = new Flowers.Flower.GrowingTips.Tempreture();
			tempreture.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());

			flower.getGrowingTips().setTempreture(tempreture);
		}

		if (currentElement.equals(XML.LIGHTING.value())) {
			Flowers.Flower.GrowingTips.Lighting lighting = new Flowers.Flower.GrowingTips.Lighting();
			lighting.setLightRequiring(startElement.getAttributeByName(new QName(XML.LIGHTREQUIRING.value())).getValue());

			flower.getGrowingTips().setLighting(lighting);
		}

		if (currentElement.equals(XML.WATERING.value())) {
			Flowers.Flower.GrowingTips.Watering watering = new Flowers.Flower.GrowingTips.Watering();
			watering.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());

			flower.getGrowingTips().setWatering(watering);
		}
	}

	private void parseContent(Characters characters) {
		if (currentElement.equals(XML.NAME.value())) {
			flower.setName(characters.getData());
		}

		if (currentElement.equals(XML.SOIL.value())) {
			flower.setSoil(characters.getData());
		}

		if (currentElement.equals(XML.ORIGIN.value())) {
			flower.setOrigin(characters.getData());
		}

		if (currentElement.equals(XML.STEMCOLOUR.value())){
			flower.getVisualParameters().setStemColour(characters.getData());
		}

		if (currentElement.equals(XML.LEAFCOLOUR.value())){
			flower.getVisualParameters().setLeafColour(characters.getData());
		}

		if (currentElement.equals(XML.AVELENFLOWER.value())) {
			flower.getVisualParameters().getAveLenFlower().setValue(new BigInteger(characters.getData()));
		}

		if (currentElement.equals(XML.TEMPRETURE.value())) {
			flower.getGrowingTips().getTempreture().setValue(new BigInteger(characters.getData()));
		}

		if (currentElement.equals(XML.WATERING.value())) {
			flower.getGrowingTips().getWatering().setValue(new BigInteger(characters.getData()));
		}

		if (currentElement.equals(XML.MULTIPLYING.value())) {
			flower.setMultiplying(characters.getData());
		}
	}

	private void parseEndElement(EndElement endElement) {
		String localName = endElement.getName().getLocalPart();

		if (localName.equals(XML.FLOWER.value())) {
			flowers.getFlower().add(flower);
		}
	}

	public Flowers getFlowers() {
		return flowers;
	}

	public void saveXML(Flowers flowers, String xmlFileName) throws TransformerException, ParserConfigurationException {
		new XMLSaver().saveXML(flowers, xmlFileName);
	}

}